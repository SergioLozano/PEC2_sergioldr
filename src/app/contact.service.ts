import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';


import { Contact } from './contact';
import { MessageService } from './message.service';



@Injectable()
export class ContactService {

  constructor(private messageService: MessageService) {

   }

  private contactListDefaults: Array<Contact> = [
      { id: 0, name: 'John', surname: 'Doe', phoneNumber: 673987555}
  ];

  contacts: Array<Contact> = []

  contactChange = new EventEmitter<Contact>();

  PERSISTENT_OBJECT_KEY:string = 'contacts';
  defaultContacts: Array<Contact> = [
      { id: 0, name: 'John', surname: 'Doe', phoneNumber: 673987555}

  ];

  getContacts(){
      let storedData: any;

      storedData = store.get(this.PERSISTENT_OBJECT_KEY);

      if(storedData){
          this.contacts = storedData;
      }else{
          store.set(this.PERSISTENT_OBJECT_KEY, this.contactListDefaults);
          for (const contact of this.defaultContacts){
              this.addContact(contact)
          }

      }
  };

 getContact(id: number) {
     let i: number;
     for (i = 0; i < this.contacts.length; i++){
         if(this.contacts[i].id === id){
             return this.contacts[i];
         }
     }
 }

  addContact(contact: Contact){
      this.contacts.push(contact);
      store.set(this.PERSISTENT_OBJECT_KEY, this.contacts);
  }

  deleteContact (id: number){
      let i: number;
      for (i = 0; i < this.contacts.length; i++){
          if(this.contacts[i].id === id){
              this.log(`Deleted ${this.contacts[i].name}`);
              this.contacts.splice(i, 1);
              store.set(this.PERSISTENT_OBJECT_KEY, this.contacts);
          }
      }
  }

  updateContact (contact: Contact){
      let i:number;

      for (i = 0; i < this.contacts.length; i++){
          if(this.contacts[i].id === contact.id){
              this.log(`Updated ${this.contacts[i].name}`);
              this.contacts[i].name = contact.name;
              this.contacts[i].surname = contact.surname;
              this.contacts[i].phoneNumber = contact.phoneNumber;
              store.set(this.PERSISTENT_OBJECT_KEY, this.contacts);
          }
      }



  }


  private log(message: string) {
    this.messageService.add('ContactService: ' + message);
  }

}
