import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'contact-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class ContactAppComponent {
  name = 'Sergio Lozano Del Río';

  constructor(
      private router: Router,
      private location: Location
    ) { }

  ngOnInit(){

  }

  goBack(): void {
      this.location.back();
  }
}
