export class Contact {
    id: number;
    name: string;
    surname: string;
    phoneNumber: number;
}
