import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Contact } from '../contact';
import { ContactService } from '../contact.service';

@Component({
  selector: 'contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {

  i:number;
  @Input() contact: Contact;
  @Output() clickDelete: EventEmitter<Contact> = new EventEmitter<Contact>();

  constructor(private contactService: ContactService) { }


  ngOnInit(){

  }

  delete() {
      this.clickDelete.emit(this.contact);
  }


}
