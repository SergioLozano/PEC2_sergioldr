import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';

import { ContactItemComponent } from '../contact-item/contact-item.component';

@Component({
  providers:[ContactItemComponent],
  selector: 'contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent {

  contact: Contact;

  @Output() clickAdd: EventEmitter<Contact> = new EventEmitter<Contact>();

  constructor(private contactService: ContactService, private contactItem: ContactItemComponent) { }

  name:string = null;
  surname:string = null;
  phoneNumber:number = null;

  clearValues() {
      this.name = '';
      this.surname = '';
      this.phoneNumber = null;
  }

  add(name: string, surname: string, phoneNumber: number) {
      let id = this.contactService.contacts.length;
      name = name.trim();
      surname = surname.trim();
      phoneNumber = phoneNumber;
      console.log(id + "" + name + "" + surname + "" + phoneNumber);
      if(!name || !surname || !phoneNumber) { return; }
      this.contact = {id, name, surname, phoneNumber};
      this.contactService.addContact(this.contact);
      this.clickAdd.emit();
      this.clearValues();
  }

}
