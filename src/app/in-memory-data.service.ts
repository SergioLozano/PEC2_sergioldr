import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const contacts = [
        { id: 0, name: 'John', surname: 'Doe', phoneNumber: '673987555'},
        { id: 1, name: 'Pepe', surname: 'Doe', phoneNumber: '673987555'},
        { id: 2, name: 'Carlos', surname: 'Doe', phoneNumber: '673987555'},
        { id: 3, name: 'Juan', surname: 'Doe', phoneNumber: '673987555'}
    ];
    return {contacts};
  }
}
