import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Contact }         from '../contact';
import { ContactService } from '../contact.service'

@Component({
  selector: 'contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {
 @Input() contact: Contact;

  constructor(
      private route: ActivatedRoute,
      private contactService: ContactService,
      private router: Router,
      private location: Location
    ) { }

  ngOnInit() {
      this.getContact();
  }

  getContact() {
      const id = +this.route.snapshot.paramMap.get('id');
      this.contact = this.contactService.getContact(id);

  }

  update() {
      this.contactService.updateContact(this.contact);
      this.location.back();
  }

  goBack(): void {
      this.location.back();
  }

  delete() {
      this.contactService.deleteContact(this.contact.id);
      this.location.back();
  }

}
